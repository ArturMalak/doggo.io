doggo.io
~ReactJS + Redux application to watch photos of dogs from Flickr API.

Author: Artur Malak

Real Application: https://doggo.arturmalak.com/

Usage:
1. "npm install" 
2. "npm  run-script build" for production build